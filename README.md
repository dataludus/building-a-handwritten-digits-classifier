The goals of this Project to explore the effectiveness of deep, feedforward neural networks at classifying images.
I compare a Multi-layer Perceptron classifier (MLP) with Linear Regression and a Random Forest models while using different parameters setup.

Check the [notebook](https://gitlab.com/dataludus/building-a-handwritten-digits-classifier/-/blob/master/Basics.ipynb)!